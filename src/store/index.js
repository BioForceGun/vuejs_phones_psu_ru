import Vue from 'vue';
import Vuex from 'vuex';
import mapper from "../utils/mapper";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        phonesData: [],
        units: [],
        lastModified: '',
        loading: false,
        inputValue: '',
        cyrillicValue: ''
    },

    // -------------------------------- G E T T E R S --------------------------------

    getters: {
        getInputValue(state) {
            return state.inputValue;
        },

        getCyrillicValue(state) {
            return state.cyrillicValue;
        },

        getLastModified(state) {
            return state.lastModified;
        },

        getPhonesData(state) {
            if (state.inputValue.length) {
                const prepareInput = state.inputValue.replace(new RegExp('-', 'g'), '').toLowerCase();
                const prepareCyrillic = state.cyrillicValue.replace(new RegExp('-', 'g'), '').toLowerCase();
                const data = state.units.filter(unit => {
                    const keys = Object.keys(unit);
                    keys.shift(); // remove 'type: unit'
                    return keys.some(key => {
                        const value = unit[key].replace(new RegExp('-', 'g'), '').toLowerCase();
                        return value.indexOf(prepareInput) > -1 || value.indexOf(prepareCyrillic) > -1;
                    });
                });

                if (!data.length) {
                    return [];
                } else {
                    return data.slice(0, 40)
                }
            } else {
                return state.phonesData;
            }
        },

        getUnits(state) {
            return state.units;
        },

        getLoading(state) {
            return state.loading
        },
    },

    // -------------------------------- M U T A T I O N S --------------------------------

    mutations: {
        setPhonesData(state, payload) {
            state.phonesData = payload;
        },

        setInputValue(state, inputString) {
            let cyrillicString = '';
            const originInput = inputString.toLowerCase().trim();

            for (const letter in originInput) {
                if (originInput[letter] in mapper) {
                    cyrillicString += mapper[originInput[letter]];
                } else {
                    cyrillicString += originInput[letter];
                }
            }

            state.inputValue = inputString;
            state.cyrillicValue = cyrillicString;
        },

        setLastModified(state, payload) {
            state.lastModified = payload;
        },

        setUnits(state, payload) {
            state.units = payload;
        },

        setLoading(state, payload) {
            state.loading = payload;
        }
    },

    // -------------------------------- A C T I O N S --------------------------------

    actions: {
        fetchPhonesData({commit}) {
            commit('setLoading', true);
            fetch('./data/data.json').then(response => {
                const lastModified = response.headers.get('Last-Modified')
                if (lastModified) {
                    commit('setLastModified', lastModified);
                }
                return response.json()
            }).then(data => {
                // Выделим все юниты из массива данных
                const unitsArr = [];
                const findUnits = (nodes, title) => {
                    nodes.forEach(node => {
                        if (node.type === 'node') {
                            findUnits(node.nodes, node.title);
                        } else {
                            unitsArr.push({ ...node, title });
                        }
                    });
                };
                findUnits(data);
                commit('setUnits', unitsArr);
                commit('setPhonesData', data)
            }).finally(() => {
                commit('setLoading', false);
            })
        }
    }
});
